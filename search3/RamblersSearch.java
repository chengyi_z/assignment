
public class RamblersSearch extends Search {
  
	private Coords goal;
  private TerrainMap map;

   public Coords getGoal() {
     return goal;
  }
  
   public TerrainMap getMap() {
	    return map;
	}
   
   public RamblersSearch(TerrainMap g , Coords m) {
	   map = g;
	   goal = m;
   }
   
   
}
