import java.util.ArrayList;

public class RamblersState extends SearchState {
	
	private Coords point;

	//constructor for AStart.
	public RamblersState(Coords i, int lc, int rc) {
		point = i;
		localCost = lc;
		estRemCost = rc;
	}

	//accessor for points.
	public Coords getPoint() {
		return point;
	}

	@Override
	boolean goalPredicate(Search searcher) {
		// TODO Auto-generated method stub
		 Coords goall = ((RamblersSearch) searcher).getGoal();
	      if( goall.gety() == point.gety() &&  goall.getx() == point.getx()) {
	    	  return true;
	      }
	      return false;
	    }

	@Override
	ArrayList<SearchState> getSuccessors(Search searcher) {
		// TODO Auto-generated method stub
		int Xaxis = point.getx();
        int Yaxis = point.gety();
		RamblersSearch ramblersSearcher = (RamblersSearch) searcher;
		TerrainMap map = ramblersSearcher.getMap();
		ArrayList<SearchState> succs = new ArrayList<SearchState>();
		
	if(0 <=  Xaxis-1) {
	//move west
		Coords point2 = new Coords(Yaxis,Xaxis-1);
		//calculate localCost
         int localCost =  1;
	     int height1 = map.getTmap()[Yaxis][Xaxis];
	     int height2 = map.getTmap()[point2.gety()][point2.getx()];
	     if (height1 <= height2) {
	     localCost = Math.abs(height2 - height1 + 1);
	    }
	     //calculate estRemCost
	     int estRemCost1 =  Math.abs(point2.getx()-point.getx()) + Math.abs(point2.gety()-point.gety());
	     if (height1 <= height2) {
	          estRemCost1 = Math.abs(height2 - height1 + estRemCost1);
	     }
		succs.add(new RamblersState(point2, localCost, estRemCost1));
	}
	
	if(Xaxis+1 <= map.getWidth()) {
	//move east
		Coords point2 = new Coords(Yaxis,Xaxis+1);
		//calculate localCost
        int localCost =  1;
		int height1 = map.getTmap()[Yaxis][Xaxis];
	    int height2 = map.getTmap()[point2.gety()][point2.getx()];
	    if (height1 <= height2) {
	        localCost = Math.abs(height2 - height1 + 1);
	    }
	    //calculate estRemCost
	     int estRemCost1 =  Math.abs(point2.getx()-point.getx()) + Math.abs(point2.gety()-point.gety());
	     if (height1 <= height2) {
	          estRemCost1 = Math.abs(height2 - height1 + estRemCost1);
	     }
		succs.add(new RamblersState(point2, localCost, estRemCost1));
	}
	
	if(Yaxis+1 <= map.getDepth()) {
	//move south
	    Coords point2 = new Coords(Yaxis+1,Xaxis);
	    //calculate localCost
        int localCost =  1;
	    int height1 = map.getTmap()[Yaxis][Xaxis];
		int height2 = map.getTmap()[point2.gety()][point2.getx()];
		if (height1 <= height2) {
		   localCost = Math.abs(height2 - height1 + 1);
		 }
		//calculate estRemCost
	     int estRemCost1 =  Math.abs(point2.getx()-point.getx()) + Math.abs(point2.gety()-point.gety());
	     if (height1 <= height2) {
	          estRemCost1 = Math.abs(height2 - height1 + estRemCost1);
	     }
		succs.add(new RamblersState(point2, localCost, estRemCost1));
	}
	
	if(0<=Yaxis-1) {
    //move north
	    Coords point2 = new Coords(Yaxis-1,Xaxis);
	    //calculate localCost
        int localCost =  1;
	    int height1 = map.getTmap()[Yaxis][Xaxis];
	    int height2 = map.getTmap()[point2.gety()][point2.getx()];
	    if (height1 <= height2) {
		    localCost = Math.abs(height2 - height1 + 1);
		 }
	    //calculate estRemCost
	     int estRemCost1 =  Math.abs(point2.getx()-point.getx()) + Math.abs(point2.gety()-point.gety());
	     if (height1 <= height2) {
	          estRemCost1 = Math.abs(height2 - height1 + estRemCost1);
	     }
		succs.add(new RamblersState(point2, localCost, estRemCost1));
	}
	  return succs;
  }


	@Override
	boolean sameState(SearchState n2) {
		// TODO Auto-generated method stub
		Coords ppoint = ((RamblersState) n2).point;
        if( ppoint.gety() == point.gety() && ppoint.getx() == point.getx()) {
        	return true;
        }
        return false;

	}
	 // toString
	  public String toString() {
	    return ("Map State: " + point.getx() + "," + point.gety());
	  }
}
