public class RunRamblersAstart {
	//runRamblersSearch, AStart search.
	public static void main(String[] arg) {
		
	  TerrainMap map1 = new TerrainMap("diablo.pgm");
	  //set goal point
	  RamblersSearch searcher = new RamblersSearch(map1, new Coords(5,8));
	  //set start point
	  SearchState initState = (SearchState) new RamblersState(new Coords(7,0), 0,0);
	  String res_bb = searcher.runSearch(initState, "A*");
	  System.out.println(res_bb);
   }
}
